## 目录
    todolist
    ├─.gitingore
    ├─app.js
    ├─app.json
    ├─app.wxss
    ├─package-lock.json
    ├─package.json
    ├─project.config.json
    ├─sitemap.json
    ├─utils
    |   ├─customPage.js
    |   └util.js
    ├─pages
    |   ├─index
    |   |   ├─index.js
    |   |   ├─index.json
    |   |   ├─index.wxml
    |   |   └index.wxss
    ├─api
    |  └index.js

project.config.json [项目配置文件](https://developers.weixin.qq.com/miniprogram/dev/devtools/projectconfig.html)

## 公共样式
    公共样式放在app.wxss 下
    案例代码:

    view{
        box-sizing: border-box;
    }

    .flex-row{
        display: flex;
        align-items: center;
    }

    .flex-row-a{
        display: flex;
        align-items: center;
        justify-content: space-around;
    }

    .flex-row-b{
        display: flex;
        align-items: center;
        justify-content: space-between;
    }

## request工具类封装
    已经将request类封装好发布npm 直接npm i dy-mp-request 安装使用
    案例代码 api/index.js

    import req from "dy-mp-request";
    let reqBody = new req();
    reqBody.beforeRequest = ({ params, extra, curPage }) => {
        // reqBody.config.loading=true
        // extra.showLoading=false
        params.url = "http://todolist.com:8888/" + params.url
        // console.log('before')
    }
    reqBody.afterRequest = ({ res, resolve, reject, params, extra }) => {
        res.statusCode == 200 ? resolve(res) : reject(res)
    }
    const api = {
        index(params = {}, extra = {}) {
            params.url = 'get'
            params.method = "GET"
            return reqBody.request(params, extra)
        },
        del(params = {}, extra = {}) {
            params.url = 'del'
            return reqBody.request(params, extra)
        },
        add(params = {}, extra = {}) {
            params.url = 'add'
            return reqBody.request(params, extra)
        },
        update(params = {}, extra = {}) {
            params.url = 'update'
            return reqBody.request(params, extra)
        }
    }
    module.exports = api;

## app.js 导入util api ...
    //app.js
    import * as util from "./utils/util"
    const api = require('./api/index')
    App({
        api,
        util
    })


