## 必备知识点

+ html css js [es6](https://es6.ruanyifeng.com/)
+ node npm
+ [官方文档](https://developers.weixin.qq.com/miniprogram/dev/framework/)

## 其他跨端开发框架
+ vue版本 [uni-app](https://uniapp.dcloud.io/)
+ react版本 [Taro](https://taro.jd.com/)

## 小程序与普通网页开发的区别

 小程序的主要开发语言是 JavaScript ，所以通常小程序的开发会被用来同普通的网页开发来做对比。两者有很大的相似性，对于前端开发者而言，从网页开发迁移到小程序的开发成本并不高，但是二者还是有些许区别的。

 网页开发渲染线程和脚本线程是互斥的，这也是为什么长时间的脚本运行可能会导致页面失去响应，而在小程序中，二者是分开的，分别运行在不同的线程中。网页开发者可以使用到各种浏览器暴露出来的 DOM API，进行 DOM 选中和操作。而如上文所述，小程序的逻辑层和渲染层是分开的，逻辑层运行在 JSCore 中，并没有一个完整浏览器对象，因而缺少相关的DOM API和BOM API。这一区别导致了前端开发非常熟悉的一些库，例如 jQuery、 Zepto 等，在小程序中是无法运行的。同时 JSCore 的环境同 NodeJS 环境也是不尽相同，所以一些 NPM 的包在小程序中也是无法运行的。

 网页开发者需要面对的环境是各式各样的浏览器，PC 端需要面对 IE、Chrome、QQ浏览器等，在移动端需要面对Safari、Chrome以及 iOS、Android 系统中的各式 WebView 。而小程序开发过程中需要面对的是两大操作系统 iOS 和 Android 的微信客户端，以及用于辅助开发的小程序开发者工具，小程序中三大运行环境也是有所区别的，如表1-1所示。

​							表1-1 小程序的运行环境

| **运行环境**     | **逻辑层**     | **渲染层**     |
| :--------------- | :------------- | :------------- |
| iOS              | JavaScriptCore | WKWebView      |
| 安卓             | X5 JSCore      | X5浏览器       |
| 小程序开发者工具 | NWJS           | Chrome WebView |

 网页开发者在开发网页的时候，只需要使用到浏览器，并且搭配上一些辅助工具或者编辑器即可。小程序的开发则有所不同，需要经过申请小程序帐号、安装小程序开发者工具、配置项目等等过程方可完成。

## 小程序的特色

 对于普通用户，小程序实现了应用的触手可及，只需要通过扫描二维码、搜索或者是朋友的分享就可以直接打开，加上优秀的体验，小程序使得服务提供者的触达能力变得更强。

 对于开发者而言，小程序框架本身所具有的快速加载和快速渲染能力，加之配套的云能力、运维能力和数据汇总能力，使得开发者不需要去处理琐碎的工作，可以把精力放置在具体的业务逻辑的开发上。

 小程序的模式使得微信可以开放更多的数据，开发者可以获取到用户的一些基本信息，甚至能够获取微信群的一些信息，使得小程序的开放能力变得更加强大。

## 小程序开发准备
+ [申请小程序账号](https://developers.weixin.qq.com/miniprogram/dev/framework/quickstart/getstart.html#%E7%94%B3%E8%AF%B7%E5%B8%90%E5%8F%B7)
+ [安装开发者工具](https://developers.weixin.qq.com/miniprogram/dev/framework/quickstart/getstart.html#%E5%AE%89%E8%A3%85%E5%BC%80%E5%8F%91%E5%B7%A5%E5%85%B7)

## 如何使用npm
[立即前往](https://developers.weixin.qq.com/miniprogram/dev/devtools/npm.html)

+ 安装node 
+ 微信开发者工具点击详情 使用npm模块
+ 项目根目录 npm init -y
+ npm i packagename 安装需要使用的npm包
+ 微信开发者工具点击工具 构建npm

