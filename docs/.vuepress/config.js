const sidebar = require('./sidebar/index.js');
// console.log(sidebar)
module.exports = {
    title: 'dy',
    base: '/blog/',
    dest:'public',
    // theme: 'reco',
    head: [
        ['link', { rel: 'icon', href: '/logo.png' }]
    ],
    themeConfig: {
        logo: '/hero.png',
        nav: [
            {
                text: '小程序入门', link: '/mp/'
            },
            { 
                text: 'contact',
                items: [
                    { text: 'gitlab', link: 'https://gitlab.com/myjustify/blog' },
                ]
            }
        ],
        sidebar
    }
}